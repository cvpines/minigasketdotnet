![MiniGasket banner](docs/header.png "MiniGasket")

# MiniGasket: A Tiny Flow-Based Programming Library

## Examples


Directing data flows:

```c#
internal static class DirectingDataFlowsExample
{
    private class StringMessage : MessageBase
    {
        public string value;

        public StringMessage(string value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return $"\"{value}\"";
        }
    }

    private class StringSource : SourceBase
    {
        public void Emit(string value)
        {
            System.Console.WriteLine($"StringSource sending message {value}");
            Send(new StringMessage(value));
        }
    }

    private class ToUpperTransformer : SourceSinkBase
    {
        public override void Receive(object sender, MessageBase message)
        {
            System.Console.WriteLine($"ToUpperTransformer got message {message} from sender {sender}");

            // Modifies the original message instead of copying. Be careful when doing this!
            StringMessage typedMessage = (StringMessage) message;
            typedMessage.value = typedMessage.value.ToUpper();
            Send(typedMessage);
        }
    }

    private class StringAppender : SourceSinkBase
    {
        private string _value;

        public StringAppender(string value)
        {
            _value = value;
        }

        public override void Receive(object sender, MessageBase message)
        {
            System.Console.WriteLine($"StringAppender got message {message} from sender {sender}");

            // Modifies the original message instead of copying. Be careful when doing this!
            StringMessage typedMessage = (StringMessage) message;
            typedMessage.value += _value;
            Send(typedMessage);
        }
    }

    private class Collector : SinkBase
    {
        private readonly List<string> _received;

        public Collector()
        {
            _received = new List<string>();
        }

        public override void Receive(object sender, MessageBase message)
        {
            System.Console.WriteLine($"Collector got message {message} from sender {sender}");

            StringMessage typedMessage = (StringMessage) message;
            _received.Add(typedMessage.value);
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            for (int i = 0; i < _received.Count; i++)
            {
                if (i != 0)
                {
                    result.Append(", ");
                }

                result.Append($"\"{_received[i]}\"");
            }
            result.Append("]");
            return result.ToString();
        }
    }

    public static void Run()
    {
        StringSource source = new StringSource();
        Collector sink = new Collector();

        source.Connect(sink);
        source.Connect(new ToUpperTransformer()).Connect(sink);
        source.Connect(new StringAppender("!")).Connect(sink);

        source.Emit("hello");
        source.Emit("world");

        // Prints ["hello", "HELLO", "hello!", "world", "WORLD", "world!"]
        System.Console.WriteLine(sink);
    }
}
```

Creating a filter:

```c#
internal static class FilterExample
{
    private class IntMessage : MessageBase
    {
        public int value;

        public IntMessage(int value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }

    private class RandomNumberSource : SourceBase
    {
        public void Emit()
        {
            Random random = new Random();
            IntMessage message = new IntMessage(random.Next() % 10);

            Send(message);
        }
    }

    private class EvenNumberFilter : FilterBase
    {
        protected override bool Predicate(object sender, MessageBase message)
        {
            IntMessage typedMessage = (IntMessage) message;
            return typedMessage.value % 2 == 0;
        }
    }

    private class Collector : SinkBase
    {
        private readonly List<int> _received;

        public Collector()
        {
            _received = new List<int>();
        }

        public override void Receive(object sender, MessageBase message)
        {
            IntMessage typedMessage = (IntMessage) message;
            _received.Add(typedMessage.value);
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            for (int i = 0; i < _received.Count; i++)
            {
                if (i != 0)
                {
                    result.Append(", ");
                }

                result.Append(_received[i].ToString());
            }
            result.Append("]");
            return result.ToString();
        }
    }

    public static void Run()
    {
        RandomNumberSource source = new RandomNumberSource();
        Collector evenSink = new Collector();
        Collector oddSink = new Collector();

        EvenNumberFilter filter = new EvenNumberFilter();
        source.Connect(filter).Connect(evenSink);
        filter.Rejected.Connect(oddSink);

        for (int i = 0; i < 10; i++)
        {
            source.Emit();
        }

        Console.WriteLine($"EVENS: {evenSink}");
        Console.WriteLine($"ODDS: {oddSink}");
    }
}
```

Multiple sources:

```c#
internal static class MultipleSourcesExample
{
    private class IntMessage : MessageBase
    {
        public int value;

        public IntMessage(int value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }

    private class IntSource : SourceBase
    {
        public void Emit(int value)
        {
            Send(new IntMessage(value));
            Send(new IntMessage(value));
        }
    }

    private class IncrementDecrement : SinkBase
    {
        public ISends Incremented => _incremented;
        public SourceProxy Decremented => _decremented;

        private readonly SourceProxy _incremented;
        private readonly SourceProxy _decremented;

        public IncrementDecrement()
        {
            _incremented = new SourceProxy(this);
            _decremented = new SourceProxy(this);
        }

        public override void Receive(object sender, MessageBase message)
        {
            IntMessage typedMessage = (IntMessage) message;
            _incremented.Send(new IntMessage(typedMessage.value + 1));
            _decremented.Send(new IntMessage(typedMessage.value - 1));
        }
    }

    private class Collector : SinkBase
    {
        private readonly List<int> _received;

        public Collector()
        {
            _received = new List<int>();
        }

        public override void Receive(object sender, MessageBase message)
        {
            IntMessage typedMessage = (IntMessage) message;
            _received.Add(typedMessage.value);
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("[");
            for (int i = 0; i < _received.Count; i++)
            {
                if (i != 0)
                {
                    result.Append(", ");
                }

                result.Append(_received[i].ToString());
            }
            result.Append("]");
            return result.ToString();
        }
    }

    public static void Run()
    {
        IntSource source = new IntSource();
        Collector incrementedSink = new Collector();
        Collector decrementedSink = new Collector();

        IncrementDecrement incdec = new IncrementDecrement();
        source.Connect(incdec);
        incdec.Incremented.Connect(incrementedSink);
        incdec.Decremented.Connect(decrementedSink);

        source.Emit(1);
        source.Emit(2);
        source.Emit(3);

        // Prints [2, 3, 4]
        Console.WriteLine($"Incremented: {incrementedSink}");

        // Prints [0, 1, 2]
        Console.WriteLine($"Decremented: {decrementedSink}");
    }
}
```

Multiple Sinks:

```c#
internal static class MultipleSinksExample
{
    private class StringMessage : MessageBase
    {
        public string value;

        public StringMessage(string value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return $"\"{value}\"";
        }
    }

    private class StringSource : SourceBase
    {
        public void Emit(string value)
        {
            Send(new StringMessage(value));
        }
    }

    private class MultiPrinter
    {
        public IReceives SinkA => _sinkA;
        public IReceives SinkB => _sinkB;

        private readonly SinkProxy _sinkA;
        private readonly SinkProxy _sinkB;

        public MultiPrinter()
        {
            _sinkA = new SinkProxy(ReceivedA);
            _sinkB = new SinkProxy(ReceivedB);
        }

        private void ReceivedA(object source, MessageBase message)
        {
            Console.WriteLine($"Got message from sink A: {message}");
        }

        private void ReceivedB(object source, MessageBase message)
        {
            Console.WriteLine($"Got message from sink B: {message}");
        }
    }

    public static void Run()
    {
        StringSource sourceA = new StringSource();
        StringSource sourceB = new StringSource();
        MultiPrinter sink = new MultiPrinter();

        sourceA.Connect(sink.SinkA);
        sourceB.Connect(sink.SinkB);

        // Prints Got message from sink A: "Hello to sink A!"
        sourceA.Emit("Hello to sink A!");

        // Prints Got message from sink B: "Hello to sink B!"
        sourceB.Emit("Hello to sink B!");
    }
}
```
