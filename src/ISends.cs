﻿namespace VioletPines.MiniGasket
{
    public interface ISends
    {
        void Send(MessageBase message);

        Pipe Connect(IReceives sink);

        void Disconnect();
    }
}
