﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// A source proxy allowing objects to expose multiple sources.
    /// </summary>
    public class SourceProxy : SourceBase
    {
        private readonly object _parent;

        public SourceProxy(object parent)
        {
            _parent = parent;
        }

        /// <summary>
        /// Sends a <paramref name="message"/> marked as originating from the proxy's parent object.
        /// </summary>
        /// <param name="message"></param>
        public new void Send(MessageBase message)
        {
            int len = _sinks.Count;
            for (int i = 0; i < len; i++)
            {
                _sinks[i].Receive(_parent, message);
            }
        }
    }
}
