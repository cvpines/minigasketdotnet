﻿using System;

namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Base exception class
    /// </summary>
    public class MiniGasketException : Exception
    {
        protected MiniGasketException(string message) : base(message)
        {

        }
    }

    /// <summary>
    /// Raised when a <see cref="Pipe"/>'s endpoint is not capable of sending messages.
    /// </summary>
    public class PipeCannotSendException : MiniGasketException
    {
        public PipeCannotSendException()
            : base("The pipe cannot send messages or connect to sinks as its endpoint does not implement ISends.")
        {

        }
    }

    /// <summary>
    /// Raised when a <see cref="Pipe"/>'s starting point is not capable of receiving messages.
    /// </summary>
    public class PipeCannotReceiveException : MiniGasketException
    {
        public PipeCannotReceiveException()
            : base("The pipe cannot receive messages as its starting point does not implement IReceives.")
        {

        }
    }
}
