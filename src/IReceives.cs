﻿namespace VioletPines.MiniGasket
{
    public delegate void ReceiveDelegate(object source, MessageBase message);


    public interface IReceives
    {
        void Receive(object sender, MessageBase message);
    }
}
