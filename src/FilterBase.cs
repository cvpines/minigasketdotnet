﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Base class for implementing a filter.
    /// Passes messages only when the predicate method returns true.
    /// </summary>
    public abstract class FilterBase : SourceSinkBase
    {
        /// <summary>
        /// Sources messages that are rejected by the filter.
        /// </summary>
        public SourceBase Rejected => _rejected;

        private readonly SourceProxy _rejected;

        protected FilterBase()
        {
            _rejected = new SourceProxy(this);
        }

        /// <summary>
        /// Determine whether or not a message should pass through the filter.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="message">The message to check.</param>
        /// <returns>True if the message should be passed, false if it should be rejected.</returns>
        protected abstract bool Predicate(object sender, MessageBase message);

        public override void Receive(object sender, MessageBase message)
        {
            if (Predicate(sender, message))
            {
                Send(message);
            }
            else
            {
                _rejected.Send(message);
            }
        }
    }
}
