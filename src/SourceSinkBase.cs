﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Base class for objects that can both send and receive messages.
    /// </summary>
    public abstract class SourceSinkBase : SourceBase, IReceives
    {
        public abstract void Receive(object sender, MessageBase message);
    }
}
