﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Represents a pipe created by connecting a sink to a source.
    /// May or may not be capable of sending and/or receiving messages, depending
    /// on the capabilities of each endpoint.
    /// </summary>
    public class Pipe : IReceives, ISends
    {
        private readonly ISends _start;
        private readonly IReceives _end;

        internal Pipe(ISends rootSource, IReceives rootSink)
        {
            _start = rootSource;
            _end = rootSink;
        }

        /// <summary>
        /// Receive a <paramref name="message"/> at the starting point.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <exception cref="PipeCannotReceiveException">
        ///     Thrown if the starting point is not an <see cref="IReceives"/>.
        /// </exception>
        public void Receive(object sender, MessageBase message)
        {
            if (_start is IReceives startAsSink)
            {
                startAsSink.Receive(sender, message);
            }
            else
            {
                throw new PipeCannotReceiveException();
            }
        }

        /// <summary>
        /// Sends a <paramref name="message"/> from the endpoint.
        /// </summary>
        /// <param name="message"></param>
        /// <exception cref="PipeCannotSendException">
        ///     Thrown if the endpoint is not an <see cref="ISends"/>.
        /// </exception>
        public void Send(MessageBase message)
        {
            if (_end is ISends endAsSource)
            {
                endAsSource.Send(message);
            }
            else
            {
                throw new PipeCannotSendException();
            }
        }

        /// <summary>
        /// Connect the pipe to a <paramref name="sink"/>.
        /// </summary>
        /// <param name="sink"></param>
        /// <returns>A new pipe from this to <paramref name="sink"/>.</returns>
        /// <exception cref="PipeCannotSendException">
        ///     Thrown if the <see cref="Pipe"/>'s endpoint is not an <see cref="ISends"/>.
        /// </exception>
        public Pipe Connect(IReceives sink)
        {
            if (_end is ISends endAsSource)
            {
                endAsSource.Connect(sink);
                return new Pipe(_start, sink);
            }
            else
            {
                throw new PipeCannotSendException();
            }
        }

        /// <summary>
        /// Disconnects all sinks from the endpoint.
        /// </summary>
        /// <exception cref="PipeCannotSendException">
        ///     Thrown if the <see cref="Pipe"/>'s endpoint is not an <see cref="ISends"/>.
        /// </exception>
        public void Disconnect()
        {
            if (_end is ISends endAsSource)
            {
                endAsSource.Disconnect();
            }
            else
            {
                throw new PipeCannotSendException();
            }
        }

        public static Pipe operator +(Pipe source, IReceives sink)
        {
            return source.Connect(sink);
        }
    }
}
