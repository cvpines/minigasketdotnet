﻿using System.Collections.Generic;

namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Base class for objects can can send messages.
    /// </summary>
    public abstract class SourceBase : ISends
    {
        protected readonly List<IReceives> _sinks;

        protected SourceBase()
        {
            _sinks = new List<IReceives>();
        }

        /// <summary>
        /// Sends a <paramref name="message"/> from the object.
        /// </summary>
        /// <param name="message"></param>
        public void Send(MessageBase message)
        {
            int len = _sinks.Count;
            for (int i = 0; i < len; i++)
            {
                _sinks[i].Receive(this, message);
            }
        }

        /// <summary>
        /// Connect this source to a sink that can receive messages.
        /// </summary>
        /// <param name="sink"></param>
        /// <returns>A <see cref="Pipe"/> holding the connection and allowing chained connections.</returns>
        public Pipe Connect(IReceives sink)
        {
            _sinks.Add(sink);
            return new Pipe(this, sink);
        }

        /// <summary>
        /// Disconnect all sinks connected to this object.
        /// </summary>
        public void Disconnect()
        {
            _sinks.Clear();
        }

        public static Pipe operator +(SourceBase source, IReceives sink)
        {
            return source.Connect(sink);
        }
    }
}
