﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// A sink proxy allowing objects to expose multiple sinks.
    /// Invokes a callback when messages are received.
    /// </summary>
    public class SinkProxy : SinkBase
    {
        private readonly ReceiveDelegate _callback;

        public SinkProxy(ReceiveDelegate callback)
        {
            _callback = callback;
        }

        public override void Receive(object sender, MessageBase message)
        {
            _callback.Invoke(sender, message);
        }
    }
}
