﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Passes through a message unchanged.
    /// </summary>
    public sealed class Passthrough : SourceSinkBase
    {
        public override void Receive(object sender, MessageBase message)
        {
            Send(message);
        }
    }
}
