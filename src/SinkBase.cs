﻿namespace VioletPines.MiniGasket
{
    /// <summary>
    /// Base class for objects that can receive messages.
    /// </summary>
    public abstract class SinkBase : IReceives
    {
        /// <summary>
        /// Receives a <seealso cref="message"/> from <seealso cref="sender"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        public abstract void Receive(object sender, MessageBase message);
    }
}
