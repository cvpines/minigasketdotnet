﻿using System;

namespace VioletPines.MiniGasket.Tests
{
    internal class MultiSource : SinkBase
    {
        public ISends SourceA => _sourceA;
        public ISends SourceB => _sourceB;

        private readonly SourceProxy _sourceA;
        private readonly SourceProxy _sourceB;

        public MultiSource()
        {
            _sourceA = new SourceProxy(this);
            _sourceB = new SourceProxy(this);
        }

        public override void Receive(object sender, MessageBase message)
        {
            StringListMessage typedMessage = (StringListMessage) message;

            StringListMessage toA = typedMessage.Clone();
            toA.path.Add(new WeakReference<ISends>(_sourceA));
            toA.value.Add("SOURCE_A");
            _sourceA.Send(toA);

            StringListMessage toB = typedMessage.Clone();
            toB.path.Add(new WeakReference<ISends>(_sourceB));
            toB.value.Add("SOURCE_B");
            _sourceB.Send(toB);
        }
    }
}
