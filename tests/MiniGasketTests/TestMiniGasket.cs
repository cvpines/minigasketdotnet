using System.Reflection;
using NUnit.Framework;

namespace VioletPines.MiniGasket.Tests
{
    public class TestMiniGasket
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestVersion()
        {
            Assembly miniGasket = Assembly.Load("MiniGasket");
            string version = miniGasket.GetName().Version?.ToString();
            Assert.AreEqual("0.2.0.0", version);
        }

        [Test]
        public void TestBasic()
        {
            Source source = new Source();
            Sink sink = new Sink();
            source.Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Source()",
                    message = new StringListMessage(source, new[]
                    {
                        "START"
                    })
                }
            }));
        }

        [Test]
        public void TestPassthrough()
        {
            Source source = new Source();
            Sink sink = new Sink();
            source.Connect(new Passthrough()).Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(source, new[]
                    {
                        "START"
                    })
                }
            }));
        }

        [Test]
        public void TestNested()
        {
            Source source = new Source();
            Sink sink = new Sink();
            source.Connect(new Appender("A").Connect(new Appender("B").Connect(sink)));

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B"
                    })
                }
            }));
        }

        [Test]
        public void TestChained()
        {
            Source source = new Source();
            Sink sink = new Sink();
            source.Connect(new Appender("A")).Connect(new Appender("B")).Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B"
                    })
                }
            }));
        }

        [Test]
        public void TestNestedPipes()
        {
            Source source = new Source();
            Sink sink = new Sink();

            Pipe bc = new Appender("B").Connect(new Appender("C"));
            Pipe de = new Appender("D").Connect(new Appender("E"));
            Pipe bcde = bc.Connect(de);
            Pipe abcdef = new Appender("A").Connect(bcde).Connect(new Appender("F"));
            source.Connect(abcdef).Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(F)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B", "C", "D", "E", "F"
                    })
                }
            }));
        }

        [Test]
        public void TestFork()
        {
            Source source = new Source();
            Sink sink1 = new Sink();
            Sink sink2 = new Sink();

            var knot = new Appender("A");
            knot.Connect(sink1);
            knot.Connect(new Appender("B")).Connect(sink2);
            source.Connect(knot);

            source.Trigger();
            Assert.That(sink1.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(A)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A"
                    })
                }
            }));
            Assert.That(sink2.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B"
                    })
                }
            }));
        }

        [Test]
        public void TestTree()
        {
            Source source = new Source();
            Sink[] sink = {
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink()
            };

            var a = source.Connect(new Appender("A"));
            a.Connect(sink[0]);
            var b = new Appender("B");
            a.Connect(b);
            b.Connect(sink[1]);
            b.Connect(new Appender("C")).Connect(sink[2]);
            var d = new Appender("D");
            d.Connect(sink[3]);
            a.Connect(d).Connect(new Appender("E")).Connect(sink[4]);
            var fg = new Appender("F").Connect(new Appender("G"));
            d.Connect(fg).Connect(sink[5]);

            source.Trigger();

            Assert.That(sink[0].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(A)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A"
                    })
                }
            }));
            Assert.That(sink[1].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B"
                    })
                }
            }));
            Assert.That(sink[2].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(C)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B", "C"
                    })
                }
            }));
            Assert.That(sink[3].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(D)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D"
                    })
                }
            }));
            Assert.That(sink[4].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(E)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D", "E"
                    })
                }
            }));
            Assert.That(sink[5].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(G)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D", "F", "G"
                    })
                }
            }));
        }


        [Test]
        public void TestDisconnect()
        {
            Source source = new Source();
            Sink sink = new Sink();

            var a = new Appender("A");
            var b = new Appender("B");

            source.Connect(a).Connect(sink);
            source.Connect(b).Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(A)",
                    message = new StringListMessage(source, new []
                    {
                        "START", "A"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new []
                    {
                        "START", "B"
                    })
                }
            }));


            source.Disconnect();

            sink.received.Clear();
            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new Sink.ReceivedEntry[]{}));
        }

        [Test]
        public void TestDisconnectPipe()
        {
            Source source = new Source();
            Sink sink = new Sink();

            var sa = source + new Appender("A");
            sa.Connect(new Appender("B")).Connect(sink);


            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new []
                    {
                        "START", "A", "B"
                    })
                }
            }));

            sa.Disconnect();

            sink.received.Clear();
            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new Sink.ReceivedEntry[]{}));
        }

        [Test]
        public void TestMultiSink()
        {
            Source source1 = new Source("1");
            Source source2 = new Source("2");
            Sink sink = new Sink();

            MultiSink multiSink = new MultiSink();
            multiSink.Connect(new Appender("COMMON")).Connect(sink);
            source1.Connect(multiSink.SinkA);
            source2.Connect(multiSink.SinkB);

            source1.Trigger();
            source2.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(COMMON)",
                    message = new StringListMessage(source1, new[]
                    {
                        "START_1", "SINK_A", "COMMON"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(COMMON)",
                    message = new StringListMessage(source2, new[]
                    {
                        "START_2", "SINK_B", "COMMON"
                    })
                }
            }));
        }

        [Test]
        public void TestPipeExceptions()
        {
            var sa = new Source() + new Appender("A");
            var bs = new Appender("B") + new Sink();

            Source source = new Source();
            source.Connect(sa);

            Assert.Throws(
                typeof(PipeCannotReceiveException),
                source.Trigger);

            Assert.Throws(
                typeof(PipeCannotSendException),
                () => bs.Connect(new Appender("C")));

            Assert.Throws(
                typeof(PipeCannotSendException),
                (bs.Disconnect));

            Assert.Throws(
                typeof(PipeCannotSendException),
                (() => bs.Send(new StringListMessage(null))));
        }

        [Test]
        public void PipeSend()
        {
            Sink sink = new Sink();

            var ab = new Appender("A") + new Appender("B");
            ab.Connect(new Appender("C")).Connect(sink);

            ab.Send(new StringListMessage(null, new []{"START"}));
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(C)",
                    message = new StringListMessage(null, new[]
                    {
                        "START", "C"
                    })
                }
            }));
        }

        [Test]
        public void TestMultiSource()
        {
            Source source = new Source();
            Sink sink1 = new Sink();
            Sink sink2 = new Sink();

            MultiSource multiSource = new MultiSource();
            source.Connect(new Appender("COM")).Connect(multiSource);

            multiSource.SourceA.Connect(new Appender("A")).Connect(new Appender("B")).Connect(sink1);
            var knot = multiSource.SourceB.Connect(new Appender("C"));
            knot.Connect(sink1);
            knot.Connect(new Appender("D")).Connect(sink2);

            source.Trigger();
            Assert.That(sink1.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_A", "A", "B"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(C)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_B", "C"
                    })
                }
            }));
            Assert.That(sink2.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(D)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_B", "C", "D"
                    })
                }
            }));
        }

        [Test]
        public void TestMultiSinkMultiSource()
        {
            Source source1 = new Source("1");
            Source source2 = new Source("2");
            Sink sink1 = new Sink();
            Sink sink2 = new Sink();

            var mss = new MultiSinkMultiSource();
            source1.Connect(new Appender("A")).Connect(mss.SinkA);
            source2.Connect(mss.SinkB);
            mss.SourceA.Connect(sink1);
            mss.SourceB.Connect(new Appender("B")).Connect(sink2);

            source1.Trigger();
            source2.Trigger();

            Assert.That(sink1.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "MultiSinkMultiSource",
                    message = new StringListMessage(source1, new[]
                    {
                        "START_1", "A", "SINK_A_TO_SOURCE_A"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "MultiSinkMultiSource",
                    message = new StringListMessage(source2, new[]
                    {
                        "START_2", "SINK_B_TO_SOURCE_A"
                    })
                }
            }));
            Assert.That(sink2.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source1, new[]
                    {
                        "START_1", "A", "SINK_A_TO_SOURCE_B", "B"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source2, new[]
                    {
                        "START_2", "SINK_B_TO_SOURCE_B", "B"
                    })
                }
            }));
        }

        [Test]
        public void TestSplitCombine()
        {
            Source source = new Source();
            Sink sink = new Sink();

            var split = new MultiSource();
            var mss = new MultiSinkMultiSource();
            var combine = new MultiSink();

            source.Connect(new Appender("COM")).Connect(split);
            split.SourceA.Connect(mss.SinkA);
            split.SourceB.Connect(mss.SinkB);
            mss.SourceA.Connect(combine.SinkA);
            mss.SourceB.Connect(combine.SinkB);
            combine.Connect(new Appender("END")).Connect(sink);

            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(END)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_A", "SINK_A_TO_SOURCE_A", "SINK_A", "END"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(END)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_A", "SINK_A_TO_SOURCE_B", "SINK_B", "END"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(END)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_B", "SINK_B_TO_SOURCE_A", "SINK_A", "END"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Appender(END)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "COM", "SOURCE_B", "SINK_B_TO_SOURCE_B", "SINK_B", "END"
                    })
                }
            }));
        }

        [Test]
        public void TestFilter()
        {
            Source sourcePass = new Source();
            Source sourceReject = new Source();
            Sink sinkAll = new Sink();
            Sink sinkPass = new Sink();
            Sink sinkReject = new Sink();

            var knot = new Passthrough();
            sourcePass.Connect(new Appender("T")).Connect(knot);
            sourceReject.Connect(new Appender("F")).Connect(knot);
            knot.Connect(sinkAll);

            var filter = new Filter();
            knot.Connect(filter);

            filter.Connect(sinkPass);
            filter.Rejected.Connect(sinkReject);

            sourcePass.Trigger();
            sourcePass.Trigger();
            sourceReject.Trigger();
            sourcePass.Trigger();
            sourceReject.Trigger();
            sourceReject.Trigger();

            Assert.That(sinkAll.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "VioletPines.MiniGasket.Passthrough",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                }
            }));
            Assert.That(sinkPass.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourcePass, new[]
                    {
                        "START", "T"
                    })
                }
            }));
            Assert.That(sinkReject.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                },
                new Sink.ReceivedEntry
                {
                    sender = "Filter",
                    message = new StringListMessage(sourceReject, new[]
                    {
                        "START", "F"
                    })
                }
            }));
        }

        [Test]
        public void TestOperator1()
        {
            Source source = new Source();
            Sink sink = new Sink();

            var bc = new Appender("B") + new Appender("C");
            var de = new Appender("D") + new Appender("E");
            var bcde = bc + de;
            var abcdef = new Appender("A") + bcde + new Appender("F");
            var _ = source + abcdef + sink;


            source.Trigger();
            Assert.That(sink.received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(F)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B", "C", "D", "E", "F"
                    })
                }
            }));
        }

        [Test]
        public void TestOperator2()
        {
            Source source = new Source();
            Sink[] sink = {
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink(),
                new Sink()
            };

            var a = source + new Appender("A");
            var b = new Appender("B");
            var d = new Appender("D");
            var fg = new Appender("F") + new Appender("G");
            var _ = new[]
            {
                a + sink[0],
                a + b,
                b + sink[1],
                b + new Appender("C") + sink[2],
                d + sink[3],
                a + d + new Appender("E") + sink[4],
                d + fg + sink[5]
            };

            source.Trigger();
            Assert.That(sink[0].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(A)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A"
                    })
                }
            }));
            Assert.That(sink[1].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(B)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B"
                    })
                }
            }));
            Assert.That(sink[2].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(C)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "B", "C"
                    })
                }
            }));
            Assert.That(sink[3].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(D)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D"
                    })
                }
            }));
            Assert.That(sink[4].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(E)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D", "E"
                    })
                }
            }));
            Assert.That(sink[5].received, Is.EquivalentTo(new[]
            {
                new Sink.ReceivedEntry
                {
                    sender = "Appender(G)",
                    message = new StringListMessage(source, new[]
                    {
                        "START", "A", "D", "F", "G"
                    })
                }
            }));
        }

        [Test]
        public void TestCyclePrevention()
        {
            Source source = new Source();

            // Self-cycle
            var a = new Appender("A");
            a.Connect(a);
            Assert.Throws(
                typeof(CycleException),
                () => a.Send(new StringListMessage(a)));

            // Injected self-cycle
            var b = new Appender("B");
            source.Connect(b).Connect(b);
            Assert.Throws(
                typeof(CycleException),
                source.Trigger);

            // SourceSink + SourceSink
            source.Disconnect();
            var c = new Appender("C");
            var d = new Appender("D");
            source.Connect(c);
            c.Connect(d);
            d.Connect(c);
            Assert.Throws(
                typeof(CycleException),
                source.Trigger);

            // SourceSink + Pipe
            var e = new Appender("E");
            var fg = new Appender("F") + new Appender("G");
            e.Connect(fg).Connect(e);
            Assert.Throws(
                typeof(CycleException),
                () => e.Send(new StringListMessage(e)));

            // Pipe + SourceSink
            source.Disconnect();
            var hi = new Appender("H") + new Appender("I");
            var j = new Appender("J");
            source.Connect(hi).Connect(j);
            j.Connect(hi);
            Assert.Throws(
                typeof(CycleException),
                source.Trigger);

            // Pipe + Pipe
            source.Disconnect();
            var kl = new Appender("K") + new Appender("L");
            var mn = new Appender("M") + new Appender("N");
            source.Connect(kl);
            kl.Connect(mn);
            mn.Connect(kl);
            Assert.Throws(
                typeof(CycleException),
                source.Trigger);
        }
    }

}
