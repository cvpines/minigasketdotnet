﻿using System;

namespace VioletPines.MiniGasket.Tests
{
    internal class Appender : SourceSinkBase
    {
        private readonly string _value;

        public Appender(string value)
        {
            _value = value;
        }

        public override void Receive(object sender, MessageBase message)
        {
            StringListMessage typedMessage = ((StringListMessage) message).Clone();

            foreach (WeakReference<ISends> reference in typedMessage.path)
            {
                if (reference.TryGetTarget(out ISends strongRef) &&
                    ReferenceEquals(strongRef, this))
                {
                    throw new CycleException();
                }
            }

            typedMessage.value.Add(_value);
            typedMessage.path.Add(new WeakReference<ISends>(this));
            Send(typedMessage);
        }

        public override string ToString()
        {
            return $"Appender({_value})";
        }
    }
}
