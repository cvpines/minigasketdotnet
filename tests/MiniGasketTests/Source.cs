﻿namespace VioletPines.MiniGasket.Tests
{
    internal class Source : SourceBase
    {
        private readonly string _value;

        public Source(string value=null)
        {
            _value = value;
        }

        public void Trigger()
        {
            StringListMessage message = new StringListMessage(this);
            message.value.Add(_value == null ? "START" : $"START_{_value}");
            Send(message);
        }

        public override string ToString()
        {
            return $"Source({_value ?? string.Empty})";
        }
    }
}
