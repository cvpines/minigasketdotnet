﻿using System;

namespace VioletPines.MiniGasket.Tests
{
    internal class MultiSinkMultiSource
    {
        public IReceives SinkA => _sinkA;
        public IReceives SinkB => _sinkB;
        public ISends SourceA => _sourceA;
        public ISends SourceB => _sourceB;

        private readonly SinkProxy _sinkA;
        private readonly SinkProxy _sinkB;
        private readonly SourceProxy _sourceA;
        private readonly SourceProxy _sourceB;

        public MultiSinkMultiSource()
        {
            _sinkA = new SinkProxy(ReceiveA);
            _sinkB = new SinkProxy(ReceiveB);
            _sourceA = new SourceProxy(this);
            _sourceB = new SourceProxy(this);
        }

        private void ReceiveA(object sender, MessageBase message)
        {
            StringListMessage typedMessage = (StringListMessage)message;
            typedMessage.path.Add(new WeakReference<ISends>(_sourceA));

            StringListMessage toA = typedMessage.Clone();
            toA.value.Add("SINK_A_TO_SOURCE_A");
            _sourceA.Send(toA);

            StringListMessage toB = typedMessage.Clone();
            toB.value.Add("SINK_A_TO_SOURCE_B");
            _sourceB.Send(toB);
        }

        private void ReceiveB(object sender, MessageBase message)
        {
            StringListMessage typedMessage = (StringListMessage)message;
            typedMessage.path.Add(new WeakReference<ISends>(_sourceB));

            StringListMessage toA = typedMessage.Clone();
            toA.value.Add("SINK_B_TO_SOURCE_A");
            _sourceA.Send(toA);

            StringListMessage toB = typedMessage.Clone();
            toB.value.Add("SINK_B_TO_SOURCE_B");
            _sourceB.Send(toB);
        }

        public override string ToString()
        {
            return "MultiSinkMultiSource";
        }
    }
}
