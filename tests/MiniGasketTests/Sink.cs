﻿using System;
using System.Collections.Generic;

namespace VioletPines.MiniGasket.Tests
{
    internal class Sink : SinkBase
    {
        public struct ReceivedEntry : IEquatable<ReceivedEntry>
        {
            public string sender;
            public StringListMessage message;

            public bool Equals(ReceivedEntry other)
            {
                return (sender.Equals(other.sender) &&
                        message.Equals(other.message));
            }

            public override string ToString()
            {
                return $"({sender}, {message})";
            }

            
        }

        public readonly List<ReceivedEntry> received;

        public Sink()
        {
            received = new List<ReceivedEntry>();
        }

        public override void Receive(object sender, MessageBase message)
        {
            received.Add(new ReceivedEntry
            {
                sender = sender.ToString(),
                message = (StringListMessage) message
            });
        }
    }
}
