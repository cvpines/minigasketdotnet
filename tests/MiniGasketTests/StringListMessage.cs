﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VioletPines.MiniGasket.Tests
{
    internal class StringListMessage : MessageBase, IEquatable<StringListMessage>
    {
        public readonly WeakReference<ISends> origin;
        public readonly List<WeakReference<ISends>> path;
        public readonly List<string> value;

        public StringListMessage(ISends origin, IEnumerable<string> value=null, IEnumerable<WeakReference<ISends>> path=null)
        {
            this.origin = new WeakReference<ISends>(origin);
            this.value = value != null ?
                new List<string>(value) :
                new List<string>();
            this.path = path != null ?
                new List<WeakReference<ISends>>(path) :
                new List<WeakReference<ISends>>();
        }

        public bool Equals(StringListMessage other)
        {
            if (other == null)
            {
                return false;
            }

            if (value.Count != other.value.Count)
            {
                return false;
            }

            int len = value.Count;
            for (int i = 0; i < len; i++)
            {
                if (!value[i].Equals(other.value[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append(origin);
            result.Append(", [");
            for (int i = 0; i < value.Count; i++)
            {
                if (i != 0)
                {
                    result.Append(", ");
                }
                result.Append(value[i]);
            }
            result.Append("]");
            return result.ToString();
        }

        public StringListMessage Clone()
        {
            origin.TryGetTarget(out ISends originRef);
            return new StringListMessage(originRef, value, path);
        }
    }
}
