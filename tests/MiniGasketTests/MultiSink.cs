﻿using System;

namespace VioletPines.MiniGasket.Tests
{
    internal class MultiSink : SourceBase
    {
        public IReceives SinkA => _sinkA;
        public IReceives SinkB => _sinkB;

        private readonly SinkProxy _sinkA;
        private readonly SinkProxy _sinkB;

        public MultiSink()
        {
            _sinkA = new SinkProxy(ReceiveA);
            _sinkB = new SinkProxy(ReceiveB);
        }

        private void ReceiveA(object source, MessageBase message)
        {
            StringListMessage typedMessage = ((StringListMessage) message).Clone();
            typedMessage.value.Add("SINK_A");
            typedMessage.path.Add(new WeakReference<ISends>(this));
            Send(typedMessage);
        }

        private void ReceiveB(object source, MessageBase message)
        {

            StringListMessage typedMessage = ((StringListMessage)message).Clone();
            typedMessage.value.Add("SINK_B");
            typedMessage.path.Add(new WeakReference<ISends>(this));
            Send(typedMessage);
        }
    }
}
