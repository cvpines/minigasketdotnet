﻿using System;

namespace VioletPines.MiniGasket.Tests
{
    internal class Filter : FilterBase
    {
        protected override bool Predicate(object sender, MessageBase message)
        {
            StringListMessage typedMessage = (StringListMessage) message;
            foreach (WeakReference<ISends> reference in typedMessage.path)
            {
                if (reference.TryGetTarget(out ISends strongRef) &&
                    ReferenceEquals(strongRef, this))
                {
                    throw new CycleException();
                }
            }

            typedMessage.path.Add(new WeakReference<ISends>(this));
            return typedMessage.value[^1].Equals("T");
        }

        public override string ToString()
        {
            return "Filter";
        }
    }
}
