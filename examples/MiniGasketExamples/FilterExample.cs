using System;
using System.Collections.Generic;
using System.Text;

namespace VioletPines.MiniGasket.Examples
{
	internal static class FilterExample
	{
		private class IntMessage : MessageBase
		{
			public int value;

			public IntMessage(int value)
			{
				this.value = value;
			}

			public override string ToString()
			{
				return value.ToString();
			}
		}

		private class RandomNumberSource : SourceBase
		{
			public void Emit()
			{
				Random random = new Random();
				IntMessage message = new IntMessage(random.Next() % 10);

				Send(message);
			}
		}

		private class EvenNumberFilter : FilterBase
		{
			protected override bool Predicate(object sender, MessageBase message)
			{
				IntMessage typedMessage = (IntMessage) message;
				return typedMessage.value % 2 == 0;
			}
		}

		private class Collector : SinkBase
		{
			private readonly List<int> _received;

			public Collector()
			{
				_received = new List<int>();
			}

			public override void Receive(object sender, MessageBase message)
			{
				IntMessage typedMessage = (IntMessage) message;
				_received.Add(typedMessage.value);
			}

			public override string ToString()
			{
				StringBuilder result = new StringBuilder("[");
				for (int i = 0; i < _received.Count; i++)
				{
					if (i != 0)
					{
						result.Append(", ");
					}

					result.Append(_received[i].ToString());
				}
				result.Append("]");
				return result.ToString();
			}
		}

		public static void Run()
		{
			RandomNumberSource source = new RandomNumberSource();
			Collector evenSink = new Collector();
			Collector oddSink = new Collector();

			EvenNumberFilter filter = new EvenNumberFilter();
			source.Connect(filter).Connect(evenSink);
			filter.Rejected.Connect(oddSink);

			for (int i = 0; i < 10; i++)
			{
				source.Emit();
			}

			Console.WriteLine($"EVENS: {evenSink}");
			Console.WriteLine($"ODDS: {oddSink}");
		}
	}
}
