using System;
using System.Collections.Generic;
using System.Text;

namespace VioletPines.MiniGasket.Examples
{
	internal static class MultipleSourcesExample
	{
		private class IntMessage : MessageBase
		{
			public int value;

			public IntMessage(int value)
			{
				this.value = value;
			}

			public override string ToString()
			{
				return value.ToString();
			}
		}

		private class IntSource : SourceBase
		{
			public void Emit(int value)
			{
				Send(new IntMessage(value));
				Send(new IntMessage(value));
			}
		}

		private class IncrementDecrement : SinkBase
		{
			public ISends Incremented => _incremented;
			public SourceProxy Decremented => _decremented;

			private readonly SourceProxy _incremented;
			private readonly SourceProxy _decremented;

			public IncrementDecrement()
			{
				_incremented = new SourceProxy(this);
				_decremented = new SourceProxy(this);
			}

			public override void Receive(object sender, MessageBase message)
			{
				IntMessage typedMessage = (IntMessage) message;
				_incremented.Send(new IntMessage(typedMessage.value + 1));
				_decremented.Send(new IntMessage(typedMessage.value - 1));
			}
		}

		private class Collector : SinkBase
		{
			private readonly List<int> _received;

			public Collector()
			{
				_received = new List<int>();
			}

			public override void Receive(object sender, MessageBase message)
			{
				IntMessage typedMessage = (IntMessage) message;
				_received.Add(typedMessage.value);
			}

			public override string ToString()
			{
				StringBuilder result = new StringBuilder("[");
				for (int i = 0; i < _received.Count; i++)
				{
					if (i != 0)
					{
						result.Append(", ");
					}

					result.Append(_received[i].ToString());
				}
				result.Append("]");
				return result.ToString();
			}
		}

		public static void Run()
		{
			IntSource source = new IntSource();
			Collector incrementedSink = new Collector();
			Collector decrementedSink = new Collector();

			IncrementDecrement incdec = new IncrementDecrement();
			source.Connect(incdec);
			incdec.Incremented.Connect(incrementedSink);
			incdec.Decremented.Connect(decrementedSink);

			source.Emit(1);
			source.Emit(2);
			source.Emit(3);

			// Prints [2, 3, 4]
			Console.WriteLine($"Incremented: {incrementedSink}");

			// Prints [0, 1, 2]
			Console.WriteLine($"Decremented: {decrementedSink}");
		}
	}
}
