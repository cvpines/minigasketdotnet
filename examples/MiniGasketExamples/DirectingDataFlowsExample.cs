using System.Collections.Generic;
using System.Text;

namespace VioletPines.MiniGasket.Examples
{
	internal static class DirectingDataFlowsExample
	{
		private class StringMessage : MessageBase
		{
			public string value;

			public StringMessage(string value)
			{
				this.value = value;
			}

			public override string ToString()
			{
				return $"\"{value}\"";
			}
		}

		private class StringSource : SourceBase
		{
			public void Emit(string value)
			{
				System.Console.WriteLine($"StringSource sending message {value}");
				Send(new StringMessage(value));
			}
		}

		private class ToUpperTransformer : SourceSinkBase
		{
			public override void Receive(object sender, MessageBase message)
			{
				System.Console.WriteLine($"ToUpperTransformer got message {message} from sender {sender}");

				// Modifies the original message instead of copying. Be careful when doing this!
				StringMessage typedMessage = (StringMessage) message;
				typedMessage.value = typedMessage.value.ToUpper();
				Send(typedMessage);
			}
		}

		private class StringAppender : SourceSinkBase
		{
			private string _value;

			public StringAppender(string value)
			{
				_value = value;
			}

			public override void Receive(object sender, MessageBase message)
			{
				System.Console.WriteLine($"StringAppender got message {message} from sender {sender}");

				// Modifies the original message instead of copying. Be careful when doing this!
				StringMessage typedMessage = (StringMessage) message;
				typedMessage.value += _value;
				Send(typedMessage);
			}
		}

		private class Collector : SinkBase
		{
			private readonly List<string> _received;

			public Collector()
			{
				_received = new List<string>();
			}

			public override void Receive(object sender, MessageBase message)
			{
				System.Console.WriteLine($"Collector got message {message} from sender {sender}");

				StringMessage typedMessage = (StringMessage) message;
				_received.Add(typedMessage.value);
			}

			public override string ToString()
			{
				StringBuilder result = new StringBuilder("[");
				for (int i = 0; i < _received.Count; i++)
				{
					if (i != 0)
					{
						result.Append(", ");
					}

					result.Append($"\"{_received[i]}\"");
				}
				result.Append("]");
				return result.ToString();
			}
		}

		public static void Run()
		{
			StringSource source = new StringSource();
			Collector sink = new Collector();

			source.Connect(sink);
			source.Connect(new ToUpperTransformer()).Connect(sink);
			source.Connect(new StringAppender("!")).Connect(sink);

			source.Emit("hello");
			source.Emit("world");

			// Prints ["hello", "HELLO", "hello!", "world", "WORLD", "world!"]
			System.Console.WriteLine(sink);
		}
	}
}
