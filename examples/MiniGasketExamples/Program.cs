﻿namespace VioletPines.MiniGasket.Examples
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			DirectingDataFlowsExample.Run();
			System.Console.WriteLine();

			FilterExample.Run();
			System.Console.WriteLine();

			MultipleSourcesExample.Run();
			System.Console.WriteLine();

			MultipleSinksExample.Run();
		}
	}
}
