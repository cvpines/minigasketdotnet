using System;

namespace VioletPines.MiniGasket.Examples
{
	internal static class MultipleSinksExample
	{
		private class StringMessage : MessageBase
		{
			public string value;

			public StringMessage(string value)
			{
				this.value = value;
			}

			public override string ToString()
			{
				return $"\"{value}\"";
			}
		}

		private class StringSource : SourceBase
		{
			public void Emit(string value)
			{
				Send(new StringMessage(value));
			}
		}

		private class MultiPrinter
		{
			public IReceives SinkA => _sinkA;
			public IReceives SinkB => _sinkB;

			private readonly SinkProxy _sinkA;
			private readonly SinkProxy _sinkB;

			public MultiPrinter()
			{
				_sinkA = new SinkProxy(ReceivedA);
				_sinkB = new SinkProxy(ReceivedB);
			}

			private void ReceivedA(object source, MessageBase message)
			{
				Console.WriteLine($"Got message from sink A: {message}");
			}

			private void ReceivedB(object source, MessageBase message)
			{
				Console.WriteLine($"Got message from sink B: {message}");
			}
		}

		public static void Run()
		{
			StringSource sourceA = new StringSource();
			StringSource sourceB = new StringSource();
			MultiPrinter sink = new MultiPrinter();

			sourceA.Connect(sink.SinkA);
			sourceB.Connect(sink.SinkB);

			// Prints Got message from sink A: "Hello to sink A!"
			sourceA.Emit("Hello to sink A!");

			// Prints Got message from sink B: "Hello to sink B!"
			sourceB.Emit("Hello to sink B!");
		}
	}
}
